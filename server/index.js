'use strict'

//Llamar las librerias de body-parser y express
const bodyParser = require('body-parser');
const express = require('express');
const multer = require('multer');


//Iniciamos express
const app = express();

//Activar las cors
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    next();
})

//Activar middleware
app.use(bodyParser.urlencoded({extended: false}));
// app.use(multer({dest: path.join(__dirname, 'public/img/uploads')}).single('image'))
app.use(bodyParser.json());
app.use(require("../routes/routes"));



module.exports = app;