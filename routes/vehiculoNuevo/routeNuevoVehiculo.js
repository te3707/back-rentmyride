const express = require('express');
const { Router } = require('express');
const modelVehiculoNuevo = require('../../models/rentMyRide/nuevoVehiculo');
const app = Router();
const Joi = require('@hapi/joi');

const schemaRegister = Joi.object({
    noSerieVehiculo: Joi.string().min(17).max(17).required(),
    marcaVehiculo: Joi.string().required(),
    modeloVehiculo: Joi.string().required(),
    tipoVehiculo: Joi.string().required(),
    anioVehiculo: Joi.number().required(),
    transmisionVehiculo: Joi.string().required(),
    colorVehiculo: Joi.string().required(),
    placasVehiculo: Joi.string().min(6).required(),
    kilometrajeVehiculo: Joi.number().required(),
    detallesVehiculo: [Joi.string().allow(null, '')],
    polizaSeguroVehiculo: Joi.string().min(10).max(13).required(),
    statusVehiculo: Joi.string().required(),
    ultimoServicioVehiculo: Joi.date().required(),
    gasolinaVehiculo: Joi.string().required(),
    precioPorDiaVehiculo: Joi.number().required(),
})

// registrar un Vehiculo
app.post('/nuevo/vehiculo', async (req, res) => {
    let body = req.body;

    //validacion
    const { error } = schemaRegister.validate(req.body)

    if (error) {
        return res.json(
            { error: error.details[0].message }
        )
    }

    const existeSerie = await modelVehiculoNuevo.findOne({ noSerieVehiculo: req.body.noSerieVehiculo })
    if (existeSerie) return res.json({ error: true, mensaje: "Vehiculo ya registrado" })

    const existePlacas = await modelVehiculoNuevo.findOne({ placasVehiculo: req.body.placasVehiculo })
    if (existePlacas) return res.json({ error: true, mensaje: "Placas ya registradas" })

    const existePoliza = await modelVehiculoNuevo.findOne({ polizaSeguroVehiculo: req.body.polizaSeguroVehiculo })
    if (existePoliza) return res.json({ error: true, mensaje: "Poliza de seguro ya registrada" })

    let registroVehiculo = new modelVehiculoNuevo({


        noSerieVehiculo: body.noSerieVehiculo,
        marcaVehiculo: body.marcaVehiculo,
        modeloVehiculo: body.modeloVehiculo,
        tipoVehiculo: body.tipoVehiculo,
        anioVehiculo: body.anioVehiculo,
        transmisionVehiculo: body.transmisionVehiculo,
        colorVehiculo: body.colorVehiculo,
        placasVehiculo: body.placasVehiculo,
        kilometrajeVehiculo: body.kilometrajeVehiculo,
        detallesVehiculo: [body.detallesVehiculo],
        polizaSeguroVehiculo: body.polizaSeguroVehiculo,
        statusVehiculo: body.statusVehiculo,
        ultimoServicioVehiculo: body.ultimoServicioVehiculo,
        gasolinaVehiculo: body.gasolinaVehiculo,
        precioPorDiaVehiculo: body.precioPorDiaVehiculo,
    });

    registroVehiculo.save();

    res.json({
        ok: true,
        registroVehiculo,
        msg: "Registro exitoso"
    });
});

// buscar Vehiculo
app.get('/obtener/vehiculos', async (req, res) => {
    const respuesta = await modelVehiculoNuevo.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/vehiculos/tipos', async (req, res) => {
    try {
        const respuesta = await modelVehiculoNuevo.find();
        const compactos = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'Compacto');
        const cantidadCompactos = compactos.length;
        const idsCompactos = compactos.map(vehiculo => vehiculo._id);
        const placasCompactos = compactos.map(vehiculo => vehiculo.placasVehiculo)
        const marcaCompactos = compactos.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloCompactos = compactos.map(vehiculo => vehiculo.modeloVehiculo)
        const anioCompactos = compactos.map(vehiculo => vehiculo.anioVehiculo)
        const statusCompactos = compactos.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaCompactos = compactos.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioCompactos = compactos.map(vehiculo => vehiculo.precioPorDiaVehiculo)




        const sedan = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'Sedan');
        const cantidadSedan = sedan.length;
        const idsSedan = sedan.map(vehiculo => vehiculo._id);
        const placasSedans = sedan.map(vehiculo => vehiculo.placasVehiculo)
        const marcaSedans = sedan.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloSedans = sedan.map(vehiculo => vehiculo.modeloVehiculo)
        const anioSedans = sedan.map(vehiculo => vehiculo.anioVehiculo)
        const statusSedans = sedan.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaSedans = sedan.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioSedans = sedan.map(vehiculo => vehiculo.precioPorDiaVehiculo)



        const SUV = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'SUV');
        const cantidadSUV = SUV.length;
        const idsSUV = SUV.map(vehiculo => vehiculo._id);
        const placasSUV = SUV.map(vehiculo => vehiculo.placasVehiculo)
        const marcaSUV = SUV.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloSUV = SUV.map(vehiculo => vehiculo.modeloVehiculo)
        const anioSUV = SUV.map(vehiculo => vehiculo.anioVehiculo)
        const statusSUV = SUV.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaSUV = SUV.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioSUV = SUV.map(vehiculo => vehiculo.precioPorDiaVehiculo)



        const camionetas = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'Camioneta');
        const cantidadCamionetas = camionetas.length;
        const idsCamionetas = camionetas.map(vehiculo => vehiculo._id);
        const placasCamionetas = camionetas.map(vehiculo => vehiculo.placasVehiculo)
        const marcaCamionetas = camionetas.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloCamionetas = camionetas.map(vehiculo => vehiculo.modeloVehiculo)
        const anioCamionetas = camionetas.map(vehiculo => vehiculo.anioVehiculo)
        const statusCamionetas = camionetas.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaCamionetas = camionetas.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioCamionetas = camionetas.map(vehiculo => vehiculo.precioPorDiaVehiculo)



        const van = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'Van');
        const cantidadVan = van.length;
        const idsVan = van.map(vehiculo => vehiculo._id);
        const placasVan = van.map(vehiculo => vehiculo.placasVehiculo)
        const marcaVan = van.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloVan = van.map(vehiculo => vehiculo.modeloVehiculo)
        const anioVan = van.map(vehiculo => vehiculo.anioVehiculo)
        const statusVan = van.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaVan = van.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioVan = van.map(vehiculo => vehiculo.precioPorDiaVehiculo)




        const lujo = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'Lujo');
        const cantidadLujo = lujo.length;
        const idsLujo = lujo.map(vehiculo => vehiculo._id);
        const placasLujo = lujo.map(vehiculo => vehiculo.placasVehiculo)
        const marcaLujo = lujo.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloLujo = lujo.map(vehiculo => vehiculo.modeloVehiculo)
        const anioLujo = lujo.map(vehiculo => vehiculo.anioVehiculo)
        const statusLujo = lujo.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaLujo = lujo.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioLujo = lujo.map(vehiculo => vehiculo.precioPorDiaVehiculo)




        const deportivos = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'Deportivo');
        const cantidadDeportivos = deportivos.length;
        const idsDeportivos = deportivos.map(vehiculo => vehiculo._id);
        const placasDeportivos = deportivos.map(vehiculo => vehiculo.placasVehiculo)
        const marcaDeportivos = deportivos.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloDeportivos = deportivos.map(vehiculo => vehiculo.modeloVehiculo)
        const anioDeportivos = deportivos.map(vehiculo => vehiculo.anioVehiculo)
        const statusDeportivos = deportivos.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaDeportivos = deportivos.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioDeportivos = deportivos.map(vehiculo => vehiculo.precioPorDiaVehiculo)



        const clasicos = respuesta.filter(vehiculo => vehiculo.tipoVehiculo === 'Clasico');
        const cantidadClasicos = clasicos.length;
        const idsClasicos = clasicos.map(vehiculo => vehiculo._id);
        const placasClasicos = clasicos.map(vehiculo => vehiculo.placasVehiculo)
        const marcaClasicos = clasicos.map(vehiculo => vehiculo.marcaVehiculo)
        const modeloClasicos = clasicos.map(vehiculo => vehiculo.modeloVehiculo)
        const anioClasicos = clasicos.map(vehiculo => vehiculo.anioVehiculo)
        const statusClasicos = clasicos.map(vehiculo => vehiculo.statusVehiculo)
        const gasolinaClasicos = clasicos.map(vehiculo => vehiculo.gasolinaVehiculo)
        const precioClasicos = clasicos.map(vehiculo => vehiculo.precioPorDiaVehiculo)

        res.status(200).json({
            ok: true,
            respuesta: {
                cantidadCompactos: cantidadCompactos,
                idsCompactos: idsCompactos,
                placasCompactos: placasCompactos,
                marcaCompactos: marcaCompactos,
                modeloCompactos: modeloCompactos,
                anioCompactos: anioCompactos,
                statusCompactos: statusCompactos,
                precioCompactos: precioCompactos,
                gasolinaCompactos: gasolinaCompactos,


                cantidadSedan: cantidadSedan,
                idsSedan: idsSedan,
                placasSedans: placasSedans,
                marcaSedans: marcaSedans,
                modeloSedans: modeloSedans,
                anioSedans: anioSedans,
                statusSedans: statusSedans,
                precioSedans: precioSedans,
                gasolinaSedans: gasolinaSedans,


                cantidadSUV: cantidadSUV,
                idsSUV: idsSUV,
                placasSUV: placasSUV,
                marcaSUV: marcaSUV,
                modeloSUV: modeloSUV,
                anioSUV: anioSUV,
                statusSUV: statusSUV,
                precioSUV: precioSUV,
                gasolinaSUV: gasolinaSUV,


                cantidadCamionetas: cantidadCamionetas,
                idsCamionetas: idsCamionetas,
                placasCamionetas: placasCamionetas,
                marcaCamionetas: marcaCamionetas,
                modeloCamionetas: modeloCamionetas,
                anioCamionetas: anioCamionetas,
                statusCamionetas: statusCamionetas,
                precioCamionetas: precioCamionetas,
                gasolinaCamionetas: gasolinaCamionetas,


                cantidadVan: cantidadVan,
                idsVan: idsVan,
                placasVan: placasVan,
                marcaVan: marcaVan,
                modeloVan: modeloVan,
                anioVan: anioVan,
                statusVan: statusVan,
                precioVan: precioVan,
                gasolinaVan: gasolinaVan,


                cantidadLujo: cantidadLujo,
                idsLujo: idsLujo,
                placasLujo: placasLujo,
                marcaLujo: marcaLujo,
                modeloLujo: modeloLujo,
                anioLujo: anioLujo,
                statusLujo: statusLujo,
                precioLujo: precioLujo,
                gasolinaLujo: gasolinaLujo,


                cantidadDeportivos: cantidadDeportivos,
                idsDeportivos: idsDeportivos,
                placasDeportivos: placasDeportivos,
                marcaDeportivos: marcaDeportivos,
                modeloDeportivos: modeloDeportivos,
                anioDeportivos: anioDeportivos,
                statusDeportivos: statusDeportivos,
                precioDeportivos: precioDeportivos,
                gasolinaDeportivos: gasolinaDeportivos,


                cantidadClasicos: cantidadClasicos,
                idsClasicos: idsClasicos,
                placasClasicos: placasClasicos,
                marcaClasicos: marcaClasicos,
                modeloClasicos: modeloClasicos,
                anioClasicos: anioClasicos,
                statusClasicos: statusClasicos,
                precioClasicos: precioClasicos,
                gasolinaClasicos: gasolinaClasicos,
            }
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error: 'Error al obtener los vehículos'
        });
    }
});


//eliminar Vehiculo
app.delete('/delete/vehiculo/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelVehiculoNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar Vehiculo
app.put('/update/vehiculo/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelVehiculoNuevo.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

//actualizar status
app.put('/update/statusvehiculo/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    delete campos.noSerieVehiculo
    delete campos.marcaVehiculo
    delete campos.modeloVehiculo
    delete campos.tipoVehiculo
    delete campos.anioVehiculo
    delete campos.transmisionVehiculo
    delete campos.colorVehiculo
    delete campos.placasVehiculo
    delete campos.kilometrajeVehiculo
    delete campos.detallesVehiculo
    delete campos.polizaSeguroVehiculo
    delete campos.ultimoServicioVehiculo
    delete campos.gasolinaVehiculo
    delete campos.pecioPorDiaVehiculo

const respuesta = await modelVehiculoNuevo.findByIdAndUpdate(id, campos, { new: true });

res.status(202).json({
    ok: true,
    respuesta,
    msg: "ACTUALIZADO EXITOSAMENTE"
});
});

//actualizar gasolina y detalles
app.put('/update/finrentavehiculo/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    delete campos.noSerieVehiculo
    delete campos.marcaVehiculo
    delete campos.modeloVehiculo
    delete campos.tipoVehiculo
    delete campos.anioVehiculo
    delete campos.transmisionVehiculo
    delete campos.colorVehiculo
    delete campos.placasVehiculo
    delete campos.kilometrajeVehiculo
    delete campos.polizaSeguroVehiculo
    delete campos.ultimoServicioVehiculo
    delete campos.pecioPorDiaVehiculo

const respuesta = await modelVehiculoNuevo.findByIdAndUpdate(id, campos, { new: true });

res.status(202).json({
    ok: true,
    respuesta,
    msg: "ACTUALIZADO EXITOSAMENTE"
});
});

module.exports = app;