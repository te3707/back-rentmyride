const express = require('express');
const { Router } = require('express');
const modelRentaNuevo = require('../../models/rentMyRide/nuevaRenta');
const app = Router();
const Joi = require('@hapi/joi');

const schemaRegister = Joi.object({
    rfcCliente: Joi.string().min(12).max(12).required(),
    fechaHoraInicioRenta: Joi.string().required(),
    fechaHoraFinRenta: Joi.string().allow(null, ''),
    placasVehiculoRenta: Joi.string().required(),
    gasolinaInicioRenta: Joi.string().required(),
    gasolinaFinRenta: Joi.string().allow(null, ''),
    statusRenta: Joi.string().required(),
    precioRenta: Joi.number().allow(null, ''),
    detallesRenta: Joi.string().allow(null, ''),
    idVehiculoRenta: Joi.string()
})

// registrar un Renta
app.post('/nuevo/renta', async (req, res) => {
    let body = req.body;

    //validacion
    const { error } = schemaRegister.validate(req.body)

    if (error) {
        return res.json(
            { error: error.details[0].message }
        )
    }

    let registroRenta = new modelRentaNuevo({

        rfcCliente: body.rfcCliente,
        fechaHoraInicioRenta: body.fechaHoraInicioRenta,
        fechaHoraFinRenta: body.fechaHoraFinRenta,
        placasVehiculoRenta: body.placasVehiculoRenta,
        gasolinaInicioRenta: body.gasolinaInicioRenta,
        gasolinaFinRenta: body.gasolinaFinRenta,
        statusRenta: body.statusRenta,
        precioRenta: body.precioRenta,
        detallesRenta: body.detallesRenta,
        idVehiculoRenta: body.idVehiculoRenta
    });

    registroRenta.save();

    res.json({
        ok: true,
        registroRenta,
        msg: "Registro exitoso"
    });
});




app.get('/obtener/rentas/status', async (req, res) => {
    try {
        const respuesta = await modelRentaNuevo.find();
        const activas = respuesta.filter(renta => renta.statusRenta === 'Activa');
        const idsActivas = activas.map(renta => renta._id);
        const rfcsRentasActivas = activas.map(renta => renta.rfcCliente);
        const fechaInicioActivas = activas.map(renta => renta.fechaHoraInicioRenta)
        const fechaFinActivas = activas.map(renta => renta.fechaHoraFinRenta)
        const placasActivas = activas.map(renta => renta.placasVehiculoRenta)
        const gasolinaInicioActivas = activas.map(renta => renta.gasolinaInicioRenta)
        const idsVehiculoActivas = activas.map(renta => renta.idVehiculoRenta)
        const gasolinaFinActivas = activas.map(renta => renta.gasolinaFinRenta)
        const statusActivas = activas.map(renta => renta.statusRenta)
        const preciosActivas = activas.map(renta => renta.precioRenta)
        const detallesActivas = activas.map(renta => renta.detallesRenta)
        const concluidas = respuesta.filter(renta => renta.statusRenta === 'Concluida');
        const rfcsRentasconcluidas = concluidas.map(renta => renta.rfcCliente);
        const fechaInicioconcluidas = concluidas.map(renta => renta.fechaHoraInicioRenta)
        const fechaFinconcluidas = concluidas.map(renta => renta.fechaHoraFinRenta)
        const placasconcluidas = concluidas.map(renta => renta.placasVehiculoRenta)
        const gasolinaInicioconcluidas = concluidas.map(renta => renta.gasolinaInicioRenta)
        const gasolinaFinconcluidas = concluidas.map(renta => renta.gasolinaFinRenta)
        const statusconcluidas = concluidas.map(renta => renta.statusRenta)
        const preciosconcluidas = concluidas.map(renta => renta.precioRenta)
        const detallesconcluidas = concluidas.map(renta => renta.detallesRenta)


        res.status(200).json({
            ok: true,
            respuesta: {
                idsActivas: idsActivas,
                rfcsRentasActivas: rfcsRentasActivas,
                fechaInicioActivas: fechaInicioActivas,
                fechaFinActivas: fechaFinActivas,
                placasActivas: placasActivas,
                gasolinaInicioActivas: gasolinaInicioActivas,
                gasolinaFinActivas: gasolinaFinActivas,
                statusActivas: statusActivas,
                preciosActivas: preciosActivas,
                detallesActivas: detallesActivas,
                idsVehiculoActivas: idsVehiculoActivas,
                rfcsRentasconcluidas: rfcsRentasconcluidas,
                fechaInicioconcluidas: fechaInicioconcluidas,
                fechaFinconcluidas: fechaFinconcluidas,
                placasconcluidas: placasconcluidas,
                gasolinaInicioconcluidas: gasolinaInicioconcluidas,
                gasolinaFinconcluidas: gasolinaFinconcluidas,
                statusconcluidas: statusconcluidas,
                preciosconcluidas: preciosconcluidas,
                detallesconcluidas: detallesconcluidas,
            }
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error: 'Error al obtener los vehículos'
        });
    }
});




// buscar Renta
app.get('/obtener/rentas', async (req, res) => {
    const respuesta = await modelRentaNuevo.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});

app.get('/obtener/rentas/tipos', async (req, res) => {
    try {
        const respuesta = await modelrentaNuevo.find();
        const compactos = respuesta.filter(renta => renta.tiporenta === 'Compacto');
        const cantidadCompactos = compactos.length;
        const idsCompactos = compactos.map(renta => renta._id);

        res.status(200).json({
            ok: true,
            respuesta: {
                cantidadCompactos: cantidadCompactos,
                idsCompactos: idsCompactos
            }
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error: 'Error al obtener los vehículos'
        });
    }
});


//eliminar renta
app.delete('/delete/renta/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelRentaNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar Renta
app.put('/update/renta/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;


    delete campos.rfcCliente
    delete campos.fechaHoraInicioRenta
    delete campos.placasVehiculoRenta
    delete campos.gasolinaInicioRenta

    const respuesta = await modelRentaNuevo.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;