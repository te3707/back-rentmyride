const express = require('express');
const { Router } = require('express');
const modelEmpleadoNuevo = require('../../models/rentMyRide/nuevoEmpleado');
const md5 = require("bcryptjs");
const app = Router();
const Joi = require('@hapi/joi');

const schemaRegister = Joi.object({
    nombresEmpleado: Joi.string().min(6).max(255).required(),
    apellidosEmpleado: Joi.string().min(6).max(255).required(),
    emailEmpleado: Joi.string().min(6).max(255).required().email(),
    roleEmpleado: Joi.string().min(6).max(255).required(),
    sueldoEmpleado: Joi.number().required(),
    rfcEmpleado: Joi.string().min(12).max(12).required(),
    passwordEmpleado: Joi.string().min(6).required(),
    statusEmpleado: Joi.string().required(),
    telefonoEmpleado: Joi.number().required()
})


const schemaLogin = Joi.object({
    emailEmpleado: Joi.string().min(6).max(255).required().email(),
    passwordEmpleado: Joi.string().min(6).required(),
})

app.post('/login', async (req, res) => {

    const { error } = schemaLogin.validate(req.body)
    if (error) return res.status(400).json({error: error.details[0].message})

    const user = await modelEmpleadoNuevo.findOne({ emailEmpleado: req.body.emailEmpleado })
    if (!user) return res.json({ ok : false, error: true, mensaje: "email no registrado" })

    const passCorrecta = await md5.compare( req.body.passwordEmpleado, user.passwordEmpleado )
    if (!passCorrecta) return res.json({ ok : false, error: true, mensaje: "contraseña mal" })

    // const token = jwt.sign({
    //     nombreUsuario: user.nombreUsuario,
    //     id: user._id,
    //     role: user.role
    // }, process.env.token_secret)

    res.json({
        ok: true,
        error: null,
        mensaje: "bienvenido",
        role: user.roleEmpleado,
        // token: token
    })
})

// registrar un empleado
app.post('/nuevo/empleado', async (req, res) => {
    let body = req.body;

    //validacion
    const { error } = schemaRegister.validate(req.body)

    if (error) {
        return res.json(
            { error: error.details[0].message }
        )
    }

    const existeEmail = await modelEmpleadoNuevo.findOne({ emailEmpleado: req.body.emailEmpleado })
    if (existeEmail) return res.json({ error: true, mensaje: "Email ya registrado" })

    const existeRFC = await modelEmpleadoNuevo.findOne({ rfcEmpleado: req.body.rfcEmpleado })
    if (existeRFC) return res.json({ error: true, mensaje: "RFC ya registrado" })

    let registroEmpleado = new modelEmpleadoNuevo({

        nombresEmpleado: body.nombresEmpleado,
        apellidosEmpleado: body.apellidosEmpleado,
        emailEmpleado: body.emailEmpleado,
        roleEmpleado: body.roleEmpleado,
        sueldoEmpleado: body.sueldoEmpleado,
        rfcEmpleado: body.rfcEmpleado,
        passwordEmpleado: md5.hashSync(body.passwordEmpleado, 10),
        statusEmpleado: body.statusEmpleado,
        telefonoEmpleado: body.telefonoEmpleado
    });

    registroEmpleado.save();

    res.json({
        ok: true,
        registroEmpleado,
        msg: "Registro exitoso"
    });
});

// buscar empleado
app.get('/obtener/empleados', async (req, res) => {
    const respuesta = await modelEmpleadoNuevo.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});

//eliminar empleado
app.delete('/delete/empleado/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelEmpleadoNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar empleado
app.put('/update/empleado/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelEmpleadoNuevo.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;