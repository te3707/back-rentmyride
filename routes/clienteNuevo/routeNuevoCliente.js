const express = require('express');
const { Router } = require('express');
const modelClienteNuevo = require('../../models/rentMyRide/nuevoCliente');
const app = Router();
const Joi = require('@hapi/joi');

const schemaRegister = Joi.object({
    nombresCliente: Joi.string().required(),
    apellidosCliente: Joi.string().required(),
    emailCliente: Joi.string().min(6).max(255).required().email(),
    rfcCliente: Joi.string().min(12).max(12).required(),
    statusCliente: Joi.string().required(),
    telefonoCliente: Joi.number().required(),
    rfcAvalCliente: Joi.string().min(12).max(12).required()
})

// registrar un Cliente
app.post('/nuevo/cliente', async (req, res) => {
    let body = req.body;

    //validacion
    const { error } = schemaRegister.validate(req.body)

    if (error) {
        return res.json(
            { error: error.details[0].message }
        )
    }

    const existeEmail = await modelClienteNuevo.findOne({ emailCliente: req.body.emailCliente })
    if (existeEmail) return res.json({ error: true, mensaje: "Email ya registrado" })

    const existeRFC = await modelClienteNuevo.findOne({ rfcCliente: req.body.rfcCliente })
    if (existeRFC) return res.json({ error: true, mensaje: "RFC ya registrado" })

    let registroCliente = new modelClienteNuevo({
        nombresCliente: body.nombresCliente,
        apellidosCliente: body.apellidosCliente,
        emailCliente: body.emailCliente,
        rfcCliente: body.rfcCliente,
        statusCliente: body.statusCliente,
        telefonoCliente: body.telefonoCliente,
        rfcAvalCliente: body.rfcAvalCliente
    })

    registroCliente.save();

    res.json({
        ok: true,
        registroCliente,
        msg: "Registro exitoso"
    });
});

// buscar Cliente
app.get('/obtener/clientes', async (req, res) => {
    const respuesta = await modelClienteNuevo.find();


    res.status(200).json({
        ok: true,
        respuesta
    });
});

//eliminar Cliente
app.delete('/delete/cliente/:id', async (req, res) => {
    let id = req.params.id;
    const respuesta = await modelClienteNuevo.findByIdAndDelete(id);

    res.status(200).json({
        ok: true,
        respuesta,
        msg: "REGISTRO ELIMINADO EXITOSAMENTE"
    });
});

//actualizar Cliente
app.put('/update/cliente/:id', async (req, res) => {
    let id = req.params.id;
    const campos = req.body;

    const respuesta = await modelClienteNuevo.findByIdAndUpdate(id, campos, { new: true });

    res.status(202).json({
        ok: true,
        respuesta,
        msg: "ACTUALIZADO EXITOSAMENTE"
    });
});

module.exports = app;