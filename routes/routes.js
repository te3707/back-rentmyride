const express = require('express');
const app = express();


app.use(require('./test'))
app.use(require('./empleadoNuevo/routeNuevoEmpleado'))
app.use(require('./clienteNuevo/routeNuevoCliente'))
app.use(require('./vehiculoNuevo/routeNuevoVehiculo'))
app.use(require('./rentaNueva/routeNuevaRenta'))



module.exports = app;