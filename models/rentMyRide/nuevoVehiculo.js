const mongoose = require('mongoose');


let Schema = mongoose.Schema;

let nuevoVehiculo = new Schema({
    noSerieVehiculo: { type: String, required: true, minlenght: 17, maxlenght: 17 },
    marcaVehiculo: { type: String, required: true },
    modeloVehiculo: { type: String, required: true },
    tipoVehiculo: { type: String, required: true },
    anioVehiculo: { type: Number, required: true },
    transmisionVehiculo: { type: String, required: true },
    colorVehiculo: { type: String, required: true },
    placasVehiculo: { type: String, required: true },
    kilometrajeVehiculo: { type: Number, required: true },
    detallesVehiculo: [{ type: String}],
    polizaSeguroVehiculo: { type: String, required: true, minlenght: 10, maxlenght: 13 },
    statusVehiculo: { type: String, required: true },
    ultimoServicioVehiculo: { type: Date, required: true },
    gasolinaVehiculo: { type: String, required: true },
    precioPorDiaVehiculo: { type: Number, required: true }
});


module.exports = mongoose.model('nuevoVehiculo', nuevoVehiculo);