const mongoose = require('mongoose');


let Schema = mongoose.Schema;

let nuevoEmpleado = new Schema({
    nombresEmpleado: { type: String, required: true},
    apellidosEmpleado: { type: String, required: true},
    emailEmpleado: { type: String, required: true, min: 6, max: 255 },
    roleEmpleado: { type: String, required: true, min: 6, max: 255 },
    sueldoEmpleado: { type: Number, required: true },
    rfcEmpleado: { type: String, required: true, minlenght: 12, maxlenght: 12 },
    passwordEmpleado: { type: String, required: true, minlenght: 6 },
    statusEmpleado: { type: String, required: true},
    telefonoEmpleado: { type: Number, required: true}
});




module.exports = mongoose.model('nuevoEmpleado', nuevoEmpleado);