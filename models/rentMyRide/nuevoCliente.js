const mongoose = require('mongoose');


let Schema = mongoose.Schema;

let nuevoCliente = new Schema({
    nombresCliente: { type: String, required: true},
    apellidosCliente: { type: String, required: true},
    emailCliente: { type: String, required: true, min: 6, max: 255 },
    rfcCliente: { type: String, required: true, minlenght: 12, maxlenght: 12 },
    statusCliente: { type: String, required: true },
    telefonoCliente: { type: Number, required: true },
    rfcAvalCliente: { type: String, required: true, minlenght: 12, maxlenght: 12 }
});




module.exports = mongoose.model('nuevoCliente', nuevoCliente);