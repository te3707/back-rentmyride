const mongoose = require('mongoose');


let Schema = mongoose.Schema;

let nuevaRenta = new Schema({
    rfcCliente: { type: String, required: true, minlenght: 12, maxlenght: 12 },
    fechaHoraInicioRenta: { type: String, required: true },
    fechaHoraFinRenta: { type: String },
    placasVehiculoRenta: { type: String, required: true },
    gasolinaInicioRenta: { type: String, required: true },
    gasolinaFinRenta: { type: String},
    statusRenta: { type: String, required: true },
    precioRenta: { type: Number },
    detallesRenta: { type: String },
    idVehiculoRenta: { type: String}
});




module.exports = mongoose.model('nuevaRenta', nuevaRenta);