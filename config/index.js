'use strict'
console.log('Nodemon ejecutando correctamente');

const mongoose = require('mongoose');
const app = require('../server/index');
const port = 3900;

// Generar romesa global
mongoose.Promise = global.Promise;

// Hacer la conexión a la DB
mongoose.connect('mongodb://localhost:27017/rentmyride', { useNewUrlParser: true })
    .then(() => {
        console.log('Base de datos corriendo');

        // Escuchar el puerto del server
        app.listen(port, () => {
            console.log(`Server corriendo en el puerto ${port}`);
        });
    });